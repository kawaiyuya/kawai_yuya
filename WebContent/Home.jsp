<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
	<script type="text/javascript">
		function deletebtn(){
			var result = window.confirm("削除しますか？");
			if(result){
				return true;
			}else{
				return false;
			}
		}
	</script>
	<link href="./css/Login.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="body">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
	       <ul>
	           <c:forEach items="${errorMessages}" var="messages">
	               <li><c:out value="${messages}" />
	           </c:forEach>
	       </ul>
		</div>
		   <c:remove var="errorMessages" scope="session"/>
	</c:if>
	
		<div class="title-box">
			<div class="title">HOME</div>
			<div class="links">
				<a href="NewMessage" class="link">新規投稿へ</a>
				<c:if test="${user.branch_id == 1}">
					<a href="Users" class="link">管理者権限画面へ</a>
				</c:if>
				<form action="Logout" method="get" class="logoutform">
					<input type="submit" value="ログアウト">
				</form>
			</div>
		</div>
			<div class="form-box">
				<form action="./" method="get">
				<label>投稿を検索</label>
					<input type="date" name="startdate" value="${ startdate}" class="date">
					<input type="date" name="finishdate" value="${ finishdate}" class="date"><br/>
					カテゴリー<input name="category" value="${category}" class="category">
					<input type="submit" value="検索">
				</form>
			</div>
		
	
	<c:forEach items="${message}" var="message">
	<div class="message-box">
		<form action="MessageDelete" method="post" onsubmit="return deletebtn()">
				<input type="hidden" value="${message.id }" name="message.id">
				
				<div class="message"><p><c:out value="${message.title}" /></p></div>
				
				<div class="message-text-box">
					
						<pre  class="message-text"><c:out value="${message.text}" /></pre>
					
				</div>
				<div class="message-user">
					<div class="category-box">
						カテゴリー：<c:out value="${message.category}" />
					</div>
					<div class="date-box">
						投稿日：<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
					
					</div>
					<div class="user-box">
						投稿者：<c:out value="${message.name}" />
					</div>
					<div>
						<c:if test="${message.user_id == user.id }">
							<div><input type="submit" value="投稿削除" ></div>
						</c:if>
					</div>
					
				</div>
				
				
		</form>
	
		<c:forEach items="${comment}" var="comment">
			<c:if test="${message.id == comment.message_id }">
				<div class="balloon">
					<pre><c:out value="${comment.text}" /></pre>
					<div class="comment-user-box">
						<c:out value="${comment.name}" /><br/>
						<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
						<c:if test="${comment.user_id == user.id }">
							<form action="CommentDelete" method="post" onsubmit="return deletebtn()">
								<input type="hidden" name="comment_id" value="${comment.id}">
								<input type="submit" value="コメント削除" >
							</form>
						</c:if>
					</div>
				</div>
			</c:if>
		</c:forEach>
	
		<div class="Comment-form">
			<form action="Comment" method="post">
				<textarea name="text" class="comment"></textarea>
				<input type="hidden" name="message_id" value="${message.id }">
				<input type="submit" value="コメントする">
		    </form>
		</div>
		
	</div>
	</c:forEach>
	
	
	</div>
</body>
</html>