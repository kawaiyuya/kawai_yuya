<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link href="./css/Login.css" rel="stylesheet" type="text/css">
	<meta charset="UTF-8">
	<title>ログイン画面</title>
</head>
<body>
	<div class="body">
	<div class="title-box">
	    	<div class="title">LOGIN</div>
	    </div>
		<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		        <ul>
		            <c:forEach items="${errorMessages}" var="message">
		                <li><c:out value="${message}" />
		            </c:forEach>
		        </ul>
		    </div>
		    <c:remove var="errorMessages" scope="session"/>
	    </c:if>
	    
	    
		<div class="login_form">
			<form action="Login" method="post">
				<div class="login">
					<div class="login_id">
						<p class="name">ログインID</p>
						<input type="text" name="login_id" value="${login_id}" class="loginId_form">
					</div>
					<div class="login_id">
						<p class="name">パスワード</p>
						<input type="password" name="password" class="loginId_form" >
					</div>
					<div class="login_id-submit">
					    <input type="submit" value="送信" class="submit" class="loginId_form-submit">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>