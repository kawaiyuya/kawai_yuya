<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>USEREDIT</title>
<link href="./css/Login.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="body UserEdit">
		<div class="title-box">
			<div class="title">USER EDIT</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="message">
			                <li><c:out value="${message}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session"/>
	    	</c:if>

		<form action="UserEdit" method="post">
			<input type="hidden" value="${editUser.id }" name="id">
			<div>名前</div>
			<input type="text" name="name" value="${editUser.name }"><br/>
			<div>ログインID</div>
			<input type="text" name="login_id" value="${editUser.login_id}"><br/>
			<div>新規パスワード</div>
			<input type="password" name="password"><br/>
			<div>確認用パスワード</div>
			<input type="password" name="password2"><br/>
			
			<c:choose>
			<c:when  test="${editUser.id != loginUser.id }">
			<div>支店</div>
			<select name="branch_id"><br/>
				<c:forEach items="${branchlist}" var="branchlist">
					<c:choose>
						<c:when test="${editUser.branch_id == branchlist.id}">
					     <option value="${branchlist.id}" selected>${branchlist.name}</option><br/>
						</c:when>
						<c:otherwise>
						 <option value="${branchlist.id}">${branchlist.name}</option><br/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			
			<div>役職</div>
			<select name="position_id">
				<c:forEach items="${positionlist}" var="positionlist">
					<c:choose>
						<c:when test="${editUser.position_id == positionlist.id}">
					     <option value="${positionlist.id}" selected>${positionlist.name}</option><br/>
						</c:when>
						<c:otherwise>
						 <option value="${positionlist.id}">${positionlist.name}</option><br/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br/>
			</c:when>
			<c:otherwise>
				<input type="hidden" value="${loginUser.position_id}" name="position_id">
				<input type="hidden" value="${loginUser.branch_id}" name="branch_id">
			</c:otherwise>
			</c:choose>
			
		    
			<input type="submit" value="更新"><br/>
			<a href="./Users" class="link">ユーザー一覧へ戻る</a>
		</form>
	</div>
</body>
</html>