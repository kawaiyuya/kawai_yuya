<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録ユーザー一覧画面</title>
	<script type="text/javascript">
	function statesbtn(){
		var result = window.confirm("実行しますか？");
		if(result){
			return true;
		}else{
			return false;
		}
	}
	</script>
	<link href="./css/Login.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="body">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
	       <ul>
	           <c:forEach items="${errorMessages}" var="messages">
	               <li><c:out value="${messages}" />
	           </c:forEach>
	       </ul>
		</div>
		   <c:remove var="errorMessages" scope="session"/>
	</c:if>

	<div class="title-box">
		<div class="title">USERS</div>
	</div>
	<a href="./SignUp"  class="link">新規登録画面へ</a><br/>
	<a href="./"  class="link">ホームへ戻る</a><br/>
	
	<table class="table">
	 <tr>
	    <th>店名</th>
	    <th>名前</th>
	    <th>役職</th>
	    <th>状態</th>
	    <th>編集</th>
	    <th>復活停止</th>
	    
	    
	 </tr>
	 
	<c:forEach items="${AllUser}" var="AllUser">
	<tr>
		<td><c:out value="${AllUser.branch_name }" /></td>
	    <td><c:out value="${AllUser.name}" /></td>
	    <td><c:out value="${AllUser.position_name }" /></td>
		
		<c:choose>
			<c:when test="${AllUser.states == 1}">
				<td><div>停止中</div></td>
			</c:when>
			<c:otherwise>
				<td><div>稼働中</div></td>
			</c:otherwise>
		</c:choose>
		    <td><a href="./UserEdit?id=${AllUser.id }"  class="link">編集</a></td>
		<c:choose>
			<c:when test="${ user.id != AllUser.id}">
				<c:choose>
					<c:when test="${AllUser.states == 0}" >
					<form action="States" method="post" onsubmit="return statesbtn()">
						<input type="hidden" name="states" value="1">
						<input type ="hidden" name="id" value="${AllUser.id}">
						<td><input type="submit" value="停止"></td>
					</form>
					</c:when>
					<c:otherwise>
					<form action="States" method="post" onsubmit="return statesbtn()">
						<input type="hidden" name="states" value="0" >
						<input type ="hidden" name="id" value="${AllUser.id}">
						<td><input type="submit" value="復活"></td>
					</form>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<td>変更できません</td>
			</c:otherwise>
		</c:choose>
	</tr>
	</c:forEach>
	</table>
	</div>
</body>
</html>