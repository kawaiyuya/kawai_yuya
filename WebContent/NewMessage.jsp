<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="./css/Login.css" rel="stylesheet" type="text/css">
<meta charset="UTF-8">
<title>新規投稿画面</title>
</head>
<body>
 <div class="body NewMessage">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
	       <ul>
	           <c:forEach items="${errorMessages}" var="messages">
	               <li><c:out value="${messages}" />
	           </c:forEach>
	       </ul>
		</div>
		   <c:remove var="errorMessages" scope="session"/>
	</c:if>
	
	<div class="title-box">
		<div class="title">NEW MESSAGE</div>
	</div>
	 <form action="NewMessage" method="post">
	     タイトル  (30文字まで)<br />
	     <input type="text" name="title" value="${title }">
	     <br />
	     
	     内容  (1000文字まで)<br />
	     <textarea name="text" cols="100" rows="5" class="tweet-box">${text }</textarea>
	     <br />
	     
	     カテゴリー  (10文字まで)<br />
	     <input type="text" name="category" value="${category }">
	     <br />
	     
	     <input type="submit" value="投稿">
	     <a href="./"  class="link">ホームに戻る</a>
	 </form>
    </div>
</body>
</html>