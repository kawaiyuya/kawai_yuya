<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
	<link href="./css/Login.css" rel="stylesheet" type="text/css">
	<meta charset="UTF-8">
	<title>新規登録画面</title>
	</head>
	<body>
	<div class="body SigunUp">
		<div class="title-box">
			<div class="title" >NEW USER</div>
		</div>
		<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		        <ul>
		            <c:forEach items="${errorMessages}" var="message">
		                <li><c:out value="${message}" />
		            </c:forEach>
		        </ul>
		    </div>
		    <c:remove var="errorMessages" scope="session"/>
    	</c:if>
    	<div class="login_form">
			<form action="SignUp" method="post">
				<div>ログインID</div>
				<input type="text" name="login_id" value="${ login_id}"/><br/>
				<div>パスワード</div>
				<input type="password" name="password"/><br/>
				<div>パスワード確認</div>
				<input type="password" name="password2"/><br/>
				<div>名前</div>
				<input type="text" name="name" value="${ name}"><br/>
				<div>支店</div>
				<select name="branch_id">
					<c:forEach items="${branchlist}" var="branchlist">
						<c:choose>
							<c:when test="${branch_id == branchlist.id}">
						     <option value="${branchlist.id}" selected>${branchlist.name}</option>
							</c:when>
							<c:otherwise>
							 <option value="${branchlist.id}">${branchlist.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
				
				<div>役職</div>
				<select name="position_id">
					<c:forEach items="${positionlist}" var="positionlist">
						<c:choose>
							<c:when test="${position_id == positionlist.id}">
						     <option value="${positionlist.id}" selected>${positionlist.name}</option>
							</c:when>
							<c:otherwise>
							 <option value="${positionlist.id}">${positionlist.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
				<input type="hidden" name="states" value="0">
				<input type="submit" value="送信">
			</form>
			</div>
			<a href="./" class="link">ホームへ戻る</a>
		</div>
	</body>
</html>