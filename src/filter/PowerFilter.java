package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/Users", "/EditUser", "/SignUp"})
public class PowerFilter implements Filter{
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
    		throws IOException,ServletException{
    	
    	HttpSession session = ((HttpServletRequest)req).getSession();
    	
        User user = (User)session.getAttribute("loginUser");
        if(!user.getPosition_id().equals("1") && !user.getPosition_id().equals("2")) {
        	
        	String message = "権限がありません";
        	session.setAttribute("errorMessages",message);
        	((HttpServletResponse)res).sendRedirect("./");
        	return ;
        }
        chain.doFilter(req, res);
    }
    
    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
    }
        