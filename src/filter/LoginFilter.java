package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{
        
        HttpSession session = ((HttpServletRequest)req).getSession();
        User user = (User)session.getAttribute("loginUser");
        String url  = ((HttpServletRequest)req).getServletPath();
        
        System.out.println(url);
        
        if(!url.equals("/Login") && !url.equals("/css/Login.css")) {
        	System.out.println(url);
        	System.out.println("通った");

        	if(user == null) {
        		String message = "ログインしてください";
            	session.setAttribute("errorMessages",message);
            	((HttpServletResponse)res).sendRedirect("./Login");
            	return ;
        	}
        }
//        
//        if(!url.equals("/WebContent/css/login")) {
//        	if(user == null) {
//        		String message = "ログインしてください。";
//            	session.setAttribute("errorMessages",message);
//            	((HttpServletResponse)res).sendRedirect("./Login");
//            	return ;
//        	}
//        }
//        
//        if(user == null && !((HttpServletRequest)req).getServletPath().equals("/Login")){
////        	System.out.println(((HttpServletRequest)req).getServletPath());
//        	String message = "ログインしてください。";
//        	session.setAttribute("errorMessages",message);
//        	((HttpServletResponse)res).sendRedirect("./Login");
//        	return ;
//        }
//        if(user == null && !((HttpServletRequest)req).getServletPath().equals("./css/Login")) {
//        	String message = "ログインしてください。";
//        	session.setAttribute("errorMessages",message);
//        	((HttpServletResponse)res).sendRedirect("./Login");
//        	return ;
//        }
        chain.doFilter(req, res);
    }

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}