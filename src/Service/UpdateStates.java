package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import Dao.userDao;
import beans.User;

public class UpdateStates {
	public void register(User user) {
	        Connection connection = null;
	        try {
	            connection = getConnection();

	            String states = user.getStates();
	            
	            userDao userDao = new userDao();
	            userDao.Statesupdate(connection, user);
	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}