package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import Dao.userDao;
import beans.User;
import utils.CipherUtil;

public class LoginService {

    public static User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            userDao userDao = new userDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);
            System.out.println(encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}