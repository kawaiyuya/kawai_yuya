package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import Dao.userDao;
import beans.User;

public class AllUserService {
	public static List<User> getAllUser() {
		
		Connection connection = null;
	    try {
	        connection = getConnection();
	        userDao userDao = new userDao();
	        List<User> AllUser = userDao.getUserList(connection);
	        commit(connection);
	        return AllUser;
	        
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }

	}
    
	public User getAllUser(User user) {
		Connection connection = null;
		try {
			connection = getConnection();
			userDao userDao = new userDao();
			User editUser = userDao.getUserEdit(connection, user);
			
			return editUser;
		}catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}

