package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import Dao.BranchDao;
import Dao.PositionDao;
import Dao.userDao;
import beans.Branch;
import beans.Position;
import beans.User;
import utils.CipherUtil;

public class UserEditService {

	public List<Branch> getBranch() {
		Connection connection = null;
		try {
			connection = getConnection();
			BranchDao BranchDao = new BranchDao();
			List<Branch> branch = BranchDao.getBranch(connection);
			
			return branch;
		}catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
		
	}
	
	public List<Position> getPosition(){
		Connection connection = null;
		try {
			connection = getConnection();
			PositionDao PositionDao = new PositionDao();
			List<Position> position = PositionDao.getPosition(connection);
			
			return position;
		}catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	public User getEditUser() {
		return null;
		
	}

	
	
	
	public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();
            if(!StringUtils.isEmpty(user.getPassword())) {
            	String encPassword = CipherUtil.encrypt(user.getPassword());
            	user.setPassword(encPassword);
             }
            userDao userDao = new userDao();
            userDao.update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}

