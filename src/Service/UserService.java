package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import Dao.userDao;
import beans.User;
import utils.CipherUtil;

public class UserService {

    public void register(User user) {
        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            userDao userDao = new userDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    
    public List<User> LoginIdCheck(String login_id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		List<User> result = new userDao().LoginIdCheck(connection,login_id);
    		return result;
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public List<User> EditCheck(String user_id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		List<User> result2 =  new userDao().EditCheck(connection,user_id);
    		return result2;
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}