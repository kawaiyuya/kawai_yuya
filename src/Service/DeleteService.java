package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import javax.servlet.http.HttpServlet;

import Dao.DeleteDao;
import beans.Message;


public class DeleteService extends HttpServlet {
	
	public void register(Message message) {
		
		Connection connection = null;
        try {
            connection = getConnection();

            DeleteDao deleteDao = new DeleteDao();
            deleteDao.deleteinsert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		
	}

	

}
