package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import Dao.MessageDao;
import Dao.UserMessageDao;
import beans.Message;
import beans.UserMessage;


public class NewMessage {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    
    
    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String category, String startdate, String finishdate) {

        Connection connection = null;
        try {
            connection = getConnection();
            if(StringUtils.isEmpty(startdate)) {
            	startdate = "2019-01-01";
            }
            if(StringUtils.isEmpty(finishdate)) {
            	Date d = new Date();
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            	finishdate = sdf.format(d);
            }
            
            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM,category, startdate, finishdate);
            
            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	
	
}