package Service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import javax.servlet.http.HttpServlet;

import Dao.CommentDeleteDao;
import beans.Comment;

public class CommentDeleteservice extends HttpServlet {
	public void register(Comment comment) {
		Connection connection = null;
		try {
            connection = getConnection();

            CommentDeleteDao CommentDeleteDao = new CommentDeleteDao();
            CommentDeleteDao.deleteinsert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}
