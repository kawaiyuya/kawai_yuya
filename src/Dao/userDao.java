package Dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;


public class userDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("  login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", states");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // logoin_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); //states
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            
            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranch_id());
            ps.setString(5, user.getPosition_id());
            ps.setString(6, user.getStates());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    
 // login_idの重複チェック(新規登録)
public List<User> LoginIdCheck(Connection connection,String login_id) {
	PreparedStatement ps = null;
	try {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT login_id FROM users WHERE login_id = ?");
		ps = connection.prepareStatement(sql.toString());
		ps.setString(1, login_id);
		
		ResultSet rs = ps.executeQuery();
        
        List<User> IdCheckList = toLoginIdcheck(rs);
       return IdCheckList;
	} catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

private List<User> toLoginIdcheck(ResultSet rs) throws SQLException{
	List<User> ret = new ArrayList <User>();
	try {
        while (rs.next()) {
        	String login_id = rs.getString("login_id");
        	User user = new User();
            user.setLogin_id(login_id);
            ret.add(user);
        }} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
        close(rs);
        }
//	System.out.println(ret);
	return ret;
	
}

// login_idの重複チェック(ユーザー編集)
public List<User> EditCheck(Connection connection,String user_id) {
	PreparedStatement ps = null;
	try {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT login_id FROM users WHERE id= ?");
		ps = connection.prepareStatement(sql.toString());
		ps.setString(1, user_id);
		
		ResultSet rs = ps.executeQuery();
        
        List<User> editCheckList = toEditCheck(rs);
//        System.out.println(editCheckList);
        return editCheckList;
	} catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

private List<User> toEditCheck(ResultSet rs) throws SQLException{
	List<User> ret = new ArrayList <User>();
	try {
        while (rs.next()) {
        	String login_id = rs.getString("login_id");
        	User user = new User();
            user.setLogin_id(login_id);
            ret.add(user);
        }} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
        close(rs);
        }
	
	return ret;
	
}



public User getUser(Connection connection, String login_id,
        String password) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE login_id= ? AND password = ? AND states =0";
        ps = connection.prepareStatement(sql);
        ps.setString(1, login_id);
        ps.setString(2, password);

        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}



// 社員一覧のDAO
public List<User> getUserList(Connection connection) {
	 PreparedStatement ps = null;
	    try {
	    	StringBuilder sql = new StringBuilder();
	    	sql.append("SELECT " );
	    	sql.append("users.id as id , ");
	    	sql.append("users.name as name , ");
	    	sql.append("users.login_id as login_id , ");
	    	sql.append("users.password as password , ");
	    	sql.append("users.branch_id as branch_id , ");
	    	sql.append("branches.name as branch_name ,");
	    	sql.append("users.position_id as position_id , ");
	    	sql.append("positions.name as position_name , ");
	    	sql.append("users.states as states  ");
	    	sql.append("FROM users ");
	    	sql.append("INNER JOIN branches ");
	    	sql.append("ON users.branch_id = branches.id ");
	    	sql.append("INNER JOIN positions ");
	    	sql.append("ON users.position_id = positions.id ");
	    	sql.append("ORDER BY branch_id ASC");
	        ps = connection.prepareStatement(sql.toString());
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toAllUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        }
	        
	        return userList;
	        
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
}



private List<User> toUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
            int id = rs.getInt("id");
            String login_id = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            String branch_id = rs.getString("branch_id");
            String position_id = rs.getString("position_id");
            String states = rs.getString("states");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");

            User user = new User();
            user.setId(id);
            user.setLogin_id(login_id);
            user.setPassword(password);
            user.setName(name);
            user.setBranch_id(branch_id);
            user.setPosition_id(position_id);
            user.setStates(states);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
}


//ユーザー一覧表示
private List<User> toAllUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
            int id = rs.getInt("id");
            String login_id = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            String branch_id = rs.getString("branch_id");
            String branch_name =rs.getString("branch_name");
            String position_id = rs.getString("position_id");
            String position_name = rs.getString("position_name");
            String states = rs.getString("states");
//            Timestamp createdDate = rs.getTimestamp("created_date");
//            Timestamp updatedDate = rs.getTimestamp("updated_date");

            User user = new User();
            
            user.setId(id);
            user.setLogin_id(login_id);
            user.setPassword(password);
            user.setName(name);
            user.setBranch_id(branch_id);
            user.setBranch_name(branch_name);
            user.setPosition_id(position_id);
            user.setPosition_name(position_name);
            user.setStates(states);
//            user.setCreatedDate(createdDate);
//            user.setUpdatedDate(updatedDate);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
}



// ユーザー編集
public User getUserEdit(Connection connection, User user) {
	PreparedStatement ps = null;
	try {
		
		int user_id = user.getId();
		String sql = "SELECT * FROM users WHERE id = ?";
		ps = connection.prepareStatement(sql);
        ps.setInt(1, user_id);
        
        ResultSet rs = ps.executeQuery();
        User editUserList = toEditUserList(rs);
        return editUserList;
	} catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
	
}

private User toEditUserList(ResultSet rs) throws SQLException {
	User user = new User();
    try {
        while (rs.next()) {
            int id = rs.getInt("id");
            String login_id = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            String branch_id = rs.getString("branch_id");
            String position_id = rs.getString("position_id");
            String states = rs.getString("states");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");
            
            user.setId(id);
            user.setLogin_id(login_id);
            user.setPassword(password);
            user.setName(name);
            user.setBranch_id(branch_id);
            user.setPosition_id(position_id);
            user.setStates(states);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);
            
        }
        return user;
    } finally {
        close(rs);
    }
}


public void update(Connection connection, User user) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append(" name = ?");
        sql.append(", login_id = ?");
        if(!StringUtils.isEmpty(user.getPassword())) {
        sql.append(", password = ?");
        }
        sql.append(", branch_id = ?");
        sql.append(", position_id = ?");
        sql.append(", updated_date = CURRENT_TIMESTAMP");
        sql.append(" WHERE");
        sql.append(" id = ?");

        ps = connection.prepareStatement(sql.toString());

        ps.setString(1, user.getName());
        ps.setString(2, user.getLogin_id());
//        ps.setString(3, user.getPassword());
//        ps.setString(4, user.getBranch_id());
//        ps.setString(5, user.getPosition_id());
//        ps.setInt(6, user.getId());
//        System.out.println("1" + user.getPassword());
        if(!StringUtils.isEmpty(user.getPassword())) {
        	ps.setString(3, user.getPassword());
        	ps.setString(4, user.getBranch_id());
            ps.setString(5, user.getPosition_id());
            ps.setInt(6, user.getId());
        } else {
        	ps.setString(3, user.getBranch_id());
            ps.setString(4, user.getPosition_id());
            ps.setInt(5, user.getId());
        }
        
        int count = ps.executeUpdate();
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }

}



public void Statesupdate(Connection connection, User user) {
    PreparedStatement ps = null;
    try {
    	StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append(" states =? ");
        sql.append(" WHERE");
        sql.append(" id = ?");
        
        ps = connection.prepareStatement(sql.toString());
        
        ps.setString(1, user.getStates());
        ps.setInt(2, user.getId());
        int count = ps.executeUpdate();
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}






}

