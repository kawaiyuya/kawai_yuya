package Dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

/**
 * Servlet implementation class DeleteDao
 */

public class DeleteDao {
	public void deleteinsert(Connection connection, Message message) {
		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages ");
            sql.append("WHERE id = ?");
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getId());
            ps.executeUpdate();
	    }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}
