package Dao;



import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> getPosition(Connection connection) {
		PreparedStatement ps = null;
		try {
	    	StringBuilder sql = new StringBuilder();
	    	sql.append("SELECT * FROM positions" );
	    	ps = connection.prepareStatement(sql.toString());
//	        System.out.println(ps);
	        ResultSet rs = ps.executeQuery();
	        List<Position> Positionlist = toPositionList(rs);
	        return Positionlist;
			
		} catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
		
		
	}
	
	private List<Position>  toPositionList(ResultSet rs) throws SQLException {
		 List<Position> ret = new ArrayList<Position>();
		    try {
		        while (rs.next()) {
		        	int id = rs.getInt("id");
		        	String name= rs.getString("name");
//		        	Timestamp createdDate = rs.getTimestamp("created_date");
//		            Timestamp updatedDate = rs.getTimestamp("updated_date");
		        	
		        	
		        	Position position = new Position();
		        	
		        	position.setId(id);
		        	position.setName(name);
//		        	branch.setCreatedDate(createdDate);
//		        	branch.setUpdatedDate(updatedDate);
		            
		        	ret.add(position);
		        	}
		        return ret;
		        } finally {
		            close(rs);
		        }
		}
}
