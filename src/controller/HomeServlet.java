package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.NewComment;
import Service.NewMessage;
import beans.User;
import beans.UserComment;
import beans.UserMessage;

/**
 * Servlet implementation class Home
 */
@WebServlet("/index.jsp")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		User user = (User)request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if(user != null) {
			isShowMessageForm = true;
		}else {
			isShowMessageForm = false;
		}
		String category = request.getParameter("category");
        String startdate = request.getParameter("startdate");
        String finishdate = request.getParameter("finishdate");

		List<UserMessage> message = new NewMessage().getMessage(category, startdate, finishdate);
		List<UserComment> comment = new NewComment().getComment();
		
		
		request.setAttribute("category", category);
		request.setAttribute("startdate", startdate);
		request.setAttribute("finishdate", finishdate);

		request.setAttribute("message", message);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("comment", comment);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("user",user);
        
		request.getRequestDispatcher("/Home.jsp").forward(request, response);
		
	}
}
