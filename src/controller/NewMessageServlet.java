package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import Service.NewMessage;
import beans.Message;
import beans.User;

/**
 * Servlet implementation class NewMessageServlet
 */
@WebServlet(urlPatterns = {"/NewMessage"})
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("NewMessage.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();
        String title =  request.getParameter("title");
		String text = request.getParameter("text");
		String category =request.getParameter("category");
        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setTitle(request.getParameter("title"));
            message.setText(request.getParameter("text"));
            message.setCategory(request.getParameter("category"));
            message.setUser_id(user.getId());
            new NewMessage().register(message);

            response.sendRedirect("./");
        } else {
        	request.setAttribute("title", title);
        	request.setAttribute("text", text);
        	request.setAttribute("category", category);
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("NewMessage.jsp").forward(request, response);
        }
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title =  request.getParameter("title");
		String text = request.getParameter("text");
		String category =request.getParameter("category");
		if(StringUtils.isEmpty(title) || StringUtils.isBlank(title)) {
			messages.add("タイトルを入力してください");
		}
		if(30 < title.length()) {
			messages.add("タイトルは30文字以下にしてください");
		}
		if(StringUtils.isEmpty(text) || StringUtils.isBlank(text)) {
			messages.add("本文を入力してください");
		}
		if(1000< text.length()) {
			messages.add("本文は1000文字以下にしてください");
		}
		if(StringUtils.isEmpty(category) || StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		}
		if(10<category.length()) {
			messages.add("カテゴリーは10文字以下にしてください");
		}
		if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}

}
