package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.AllUserService;
import beans.User;

/**
 * Servlet implementation class AllUser
 */
@WebServlet(urlPatterns = {"/Users"})
public class AllUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("loginUser");
        List<User> AllUser = AllUserService.getAllUser();

        request.setAttribute("user", user);
        request.setAttribute("AllUser", AllUser);
		request.getRequestDispatcher("AllUser.jsp").forward(request, response);
	}
}
