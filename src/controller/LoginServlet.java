package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import Service.LoginService;
import beans.User;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Login"})
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("Login.jsp").forward(request, response);
    }
    
    @Override 
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        User user = LoginService.login(login_id, password);

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        if (user != null) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else{
        	if(StringUtils.isBlank(login_id) || StringUtils.isBlank(password)) {
        		messages.add("ログインIDまたはパスワードが入力されていません");
        	}else if(user == null ) {
        		messages.add("ログインIDまたはパスワードが誤っています");
    		}
            session.setAttribute("errorMessages", messages);
        	request.setAttribute("login_id",login_id);
            request.setAttribute("password",password);
        	request.getRequestDispatcher("Login.jsp").forward(request, response);
        }
    }
}