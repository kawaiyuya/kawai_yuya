package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import Service.AllUserService;
import Service.UserEditService;
import Service.UserService;
import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;



@WebServlet(urlPatterns = {"/UserEdit"})
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		User user = new User();
		HttpSession session = request.getSession();
		String id = request.getParameter("id");
		List<String> messages = new ArrayList<String>();
		if(id == null) {
            session.setAttribute("errorMessages", messages);
			response.sendRedirect("Users");
			return ;
		}
		if(!Pattern.matches("^[0-9]+$", id) || id.length() > 10 ) {
            session.setAttribute("errorMessages", messages);
			messages.add("不正なパラメーターが入力されました");
			response.sendRedirect("Users");
			return ;
		}
		user.setId(Integer.parseInt(id));
		User userlist = new AllUserService().getAllUser(user);
//		System.out.println("ユーザーリスト"+ userlist.getName()); //userがnullのときに通さないようにする
		if(userlist.getName() == null) {
			session.setAttribute("errorMessages", messages);
			messages.add("不正なパラメーターが入力されました");
			response.sendRedirect("Users");
			return ;
		}
		new AllUserService().getAllUser(user);
		List<Branch> branchlist =  new UserEditService().getBranch();
		List<Position> positionlist =  new UserEditService().getPosition();
		
		request.setAttribute("loginUser", loginUser);
		request.setAttribute("editUser", userlist);
		request.setAttribute("branchlist", branchlist);
		request.setAttribute("positionlist", positionlist);
		request.getRequestDispatcher("UserEdit.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) 
					throws ServletException, IOException {
		
		List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);
        String branch_id = request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");

        
        if (isValid(request, messages) == true) {

            try {
                new UserEditService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("UserEdit.jsp").forward(request, response);
                return;
            }

//            session.setAttribute("loginUser", editUser);
            response.sendRedirect("Users");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            List<Branch> branchlist =  new UserEditService().getBranch();
    		List<Position> positionlist =  new UserEditService().getPosition();
            request.setAttribute("branchlist", branchlist);
    		request.setAttribute("positionlist", positionlist);
//    		request.setAttribute("branch_id", branch_id);
//     		request.setAttribute("position_id", position_id);
            request.getRequestDispatcher("UserEdit.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {
        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setPosition_id(request.getParameter("position_id"));
        editUser.setBranch_id(request.getParameter("branch_id"));
        editUser.setStates(request.getParameter("states"));
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String name = request.getParameter("name");
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String user_id = request.getParameter("id");
        String branch_id = request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
        List<User> result1 = new UserService().LoginIdCheck(login_id);
        List<User> result2 = new UserService().EditCheck(user_id);
        
//        System.out.println((!position_id.equals("1")));
        
        System.out.println(branch_id);
        System.out.println(position_id);
//        System.out.println(result2.get(0).getLogin_id());
//        System.out.println(!login_id.equals(result1));
//        System.out.println(result2.size() == 1);
        if (StringUtils.isBlank(name)) {
            messages.add("ユーザー名を入力してください");
        }
        else if(name.length() > 10) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        
        }
        if(StringUtils.isBlank(login_id)) {
        	messages.add("ログインIDを入力してください");
        }
        else if(!login_id.matches("^[0-9a-zA-Z]+$")) {
        	messages.add("ログインIDを正しく入力してください");
        }
        else if(login_id.length() < 6) {
        	messages.add("ログインIDは6文字以上で入力してください");
        }
        else if(login_id.length() > 20) {
        	messages.add("ログインIDは20文字以下で入力してください");
        }
        if(!login_id.equals(result2.get(0).getLogin_id()) && result1.size() == 1) {
        	messages.add("そのログインIDはすでに使用されています");
        }
        else if(!StringUtils.isBlank(password) && (!password.matches("^[0-9a-zA-Z]+$") || !password2.matches("^[0-9a-zA-Z]+$"))) {
        	messages.add("入力したパスワードまたは確認用パスワードを正しく入力してください");
        }
        else if(!StringUtils.isBlank(password) && password.length() > 20) {
        	messages.add("パスワードは20文字以下で入力してください");
        }
        else if(!StringUtils.isBlank(password) && password.length() < 6){
        	messages.add("パスワードは6文字以上で入力してください");
        }
        else if(!StringUtils.isBlank(password) && !password.equals(password2)) {
        	messages.add("入力したパスワードと確認用パスワードが一致しません");
        }
        
        if(branch_id.equals("1")) {
        	if(!position_id.equals("1") && !position_id.equals("2") ) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }else {
        	if(position_id.equals("1") || position_id.equals("2")) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }
        
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

