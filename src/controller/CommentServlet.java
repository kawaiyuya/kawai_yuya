package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import Service.CommentService;
import beans.Comment;
import beans.User;

@WebServlet("/Comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		
		if (isValid(request, messages) == true) {
			
            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setText(request.getParameter("text"));
            comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));
            comment.setUser_id(user.getId());
            
            new CommentService().register(comment);
            
            request.setAttribute("comment", comment);
            response.sendRedirect("./");
        }  else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
		
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		
		String text = request.getParameter("text");
		if(500 <= text.length()) {
			messages.add("本文は500文字以下で入力してください");
		}
		if(StringUtils.isBlank(text)) {
			messages.add("コメントを入力してください");
		}
		if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}

}
