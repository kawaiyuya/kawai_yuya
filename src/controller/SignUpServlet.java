package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import Service.UserEditService;
import Service.UserService;
import beans.Branch;
import beans.Position;
import beans.User;

@WebServlet(urlPatterns = {"/SignUp"})

public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	List<Branch> branchlist =  new UserEditService().getBranch();
		List<Position> positionlist =  new UserEditService().getPosition();
		
		
		request.setAttribute("branchlist", branchlist);
		request.setAttribute("positionlist", positionlist);
        request.getRequestDispatcher("Signup.jsp").forward(request, response);
        
    }
    
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	List<String> messages = new ArrayList<String>();
    	HttpSession session = request.getSession();
    	
    	if(isValid(request, messages) == true) {
    		
    		User user = new User();
    		
    		user.setLogin_id(request.getParameter("login_id"));
    		user.setPassword(request.getParameter("password"));
    		user.setName(request.getParameter("name"));
    		user.setBranch_id(request.getParameter("branch_id"));
    		user.setPosition_id(request.getParameter("position_id"));
    		user.setStates(request.getParameter("states"));
    		
    		new Service.UserService().register(user);
    		
    		response.sendRedirect("./Users");
    	}else {
    		List<Branch> branchlist =  new UserEditService().getBranch();
    		List<Position> positionlist =  new UserEditService().getPosition();
            request.setAttribute("branchlist", branchlist);
    		request.setAttribute("positionlist", positionlist);
    		request.setAttribute("login_id", request.getParameter("login_id"));
    		request.setAttribute("name", request.getParameter("name"));
    		request.setAttribute("branch_id", request.getParameter("branch_id"));
    		request.setAttribute("position_id", request.getParameter("position_id"));
    		request.setAttribute("states", request.getParameter("states"));
    		session.setAttribute("errorMessages", messages);
    		request.getRequestDispatcher("Signup.jsp").forward(request, response);
    	}
    }
    
    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String branch_id = request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
        String states = request.getParameter("states");
        
        List<User> result = new UserService().LoginIdCheck(login_id);
//        System.out.println(branch_id);
//      
        if(result.size() == 1) {
        	messages.add("そのログインIDはすでに使用されています。");
        }
        if (StringUtils.isBlank(login_id)) {
            messages.add("ログインIDを入力してください");
        }
        else if(!login_id.matches("^[0-9a-zA-Z]+$")) {
        	messages.add("ログインIDを正しく入力してください");
        }
        else if(login_id.length() > 20 ) {
        	messages.add("ログインIDは20文字以下で入力してください");
        }
        else if(login_id.length() < 6) {
        	messages.add("ログインIDは6文字以上で入力してください");
        }
        if (StringUtils.isBlank(password) || StringUtils.isBlank(password2)) {
            messages.add("パスワードまたは確認用パスワードが入力されていません");
        }else if(!password.equals(password2)) {
        	 messages.add("入力したパスワードと確認用パスワードが一致しません");
        }
        else if(password.length() > 20) {
        	messages.add("パスワードは20文字以下で入力してください");
        }
        else if(password.length() < 6) {
        	messages.add("パスワードは6文字以上で入力してください");
        }
        else if(!password.matches("^[0-9a-zA-Z]+$")) {
        	messages.add("パスワードを正しく入力してください");
        }
        if(StringUtils.isBlank(name)) {
        	messages.add("名前を入力してください");
        }
        if(name.length() > 20) {
        	messages.add("名前は20文字以下で入力してください");
        }
        if(StringUtils.isBlank(branch_id)) {
        	messages.add("支店を選択してください");
        }
        if(StringUtils.isBlank(position_id)) {
        	messages.add("役職を選択してください");
        }
        
        if(branch_id.equals("1")) {
        	if(!position_id.equals("1") && !position_id.equals("2") ) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }else {
        	if(position_id.equals("1") || position_id.equals("2")) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

